#!/bin/bash

if [[ $EUID -eq 0 ]]; then
  echo "This script must NOT be run as root but as the GNOME session owner."
  exit 1
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

sudo ln -s "${DIR}/screen-locker@tails.boum.org" "/usr/share/gnome-shell/extensions/"
gnome-shell --replace &
# Wait a few seconds until gnome-shell has restarted
sleep 5
gnome-shell-extension-tool -e screen-locker@tails.boum.org
