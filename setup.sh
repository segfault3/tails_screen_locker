#!/bin/bash

if [[ $EUID -ne 0 ]]; then
  echo "This script must be run as root."
  exit 1
fi

install -m 755 -d /usr/share/tails-screen-locker 
install -m 755 -t /usr/share/tails-screen-locker tails_screen_locker.py 
install -m 644 -t /usr/share/tails-screen-locker set_password_dialog.py set_password_dialog.glade
install -m 755 -t /usr/bin tails-screen-locker
